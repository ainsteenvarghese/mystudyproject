package com.example.dummyapp.dataClass

data class AlbumsItem(
    val albumId: Int,
    val id: Int,
    val thumbnailUrl: String,
    val title: String,
    val url: String
)