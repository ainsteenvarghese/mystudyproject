package com.example.dummyapp

import com.example.dummyapp.dataClass.Albums
import com.example.dummyapp.dataClass.AlbumsItem
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface JsonApi {
    @GET("photos")
   suspend fun getPost():Response<ArrayList<AlbumsItem>>

    @GET("photos/{id}")
    suspend fun getSpecificPost(@Path("id") id :Int):Response<AlbumsItem>
}