package com.example.dummyapp.activity

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.lifecycleScope
import com.example.dummyapp.R
import com.example.dummyapp.RetroInstance
import com.example.dummyapp.dataClass.AlbumsItem
import com.example.dummyapp.databinding.ActivityDetailBinding
import com.example.dummyapp.loadApiImage
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class DetailActivity : AppCompatActivity() {
    private lateinit var binding :ActivityDetailBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this,R.layout.activity_detail)

        val sharedPreferences=getSharedPreferences("id" ,  Context.MODE_PRIVATE)
        lifecycleScope.launch(Dispatchers.IO) {
            try {
                val response =
                    RetroInstance.api.getSpecificPost(sharedPreferences.getInt("imageid",0))
                if ((response.isSuccessful) && (response.body() != null)) {
                    lifecycleScope.launch(Dispatchers.Main) {
                        binding.imageView3.loadApiImage(response.body()!!.thumbnailUrl)
                        binding.textView2.text = response.body()!!.title
                    }

                }
            } catch (e: Exception) {
            }
        }

    }
}