package com.example.dummyapp.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.example.dummyapp.viewModels.LoginModelClass
import com.example.dummyapp.viewModelFactory.LoginVMFactory
import com.example.dummyapp.R
import com.example.dummyapp.databinding.LoginBindingActivity

class LoginActivity : AppCompatActivity() {
    private lateinit var binding: LoginBindingActivity
    private lateinit var loginModelClass: LoginModelClass
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this , R.layout.activity_login)

        val viewModelFactory = LoginVMFactory(this)
        loginModelClass = ViewModelProvider(this, viewModelFactory )[LoginModelClass::class.java]
        binding.loginModel = loginModelClass
        binding.lifecycleOwner = this

        loginModelClass.loginAccess.observe(this) {
            if (it == true) {
                startActivity(Intent(this@LoginActivity, HomePageActivity::class.java))
            }
        }
    }



}