package com.example.dummyapp.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.dummyapp.R
import com.example.dummyapp.RetroInstance
import com.example.dummyapp.adapterClass.UserAdapter
import com.example.dummyapp.dataClass.AlbumsItem
import com.example.dummyapp.databinding.RecycleBind
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class HomePageActivity : AppCompatActivity() {
    private lateinit var binder: RecycleBind
    lateinit var data: ArrayList<AlbumsItem>
    lateinit var datacopy: AlbumsItem
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binder = DataBindingUtil.setContentView(this, R.layout.activity_home_page)
        data = ArrayList()

        lifecycleScope.launch(Dispatchers.IO) {
            try {
                val response =
                    RetroInstance.api.getPost()

                if ((response.isSuccessful) && (response.body() != null)) {
                    data = response.body() as ArrayList<AlbumsItem>


                    Log.d("tagged", "" + data)
                    lifecycleScope.launch(Dispatchers.Main) {
                        val adapter = UserAdapter( this@HomePageActivity,data)
                        binder.rv.layoutManager = LinearLayoutManager(
                            this@HomePageActivity,
                            LinearLayoutManager.VERTICAL,
                            false
                        )
                        binder.rv.adapter = adapter
                    }
                }

            } catch (e: Exception) {
            }


        }


    }
}