package com.example.dummyapp.viewModels

import android.annotation.SuppressLint
import android.content.Context
import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class LoginModelClass(@SuppressLint("StaticFieldLeak") var context: Context) : ViewModel(), View.OnClickListener {

    var userName = String()
    var userPassword =  String()
    var loginAccess = MutableLiveData<Boolean>()


    override fun onClick(p0: View?) {
        p0?.setOnClickListener {
            loginAccess.value = (userName.isNotEmpty()) && (userPassword.isNotEmpty())

        }
    }
}