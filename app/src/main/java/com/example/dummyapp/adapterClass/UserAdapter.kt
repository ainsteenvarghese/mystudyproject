package com.example.dummyapp.adapterClass

import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.dummyapp.R
import com.example.dummyapp.RetroInstance
import com.example.dummyapp.activity.DetailActivity
import com.example.dummyapp.dataClass.Albums
import com.example.dummyapp.dataClass.AlbumsItem
import com.example.dummyapp.databinding.UserRowLayout
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class UserAdapter(var context: Context, var userItem: ArrayList<AlbumsItem>) :
    RecyclerView.Adapter<UserAdapter.MyViewHolder>() {
    private lateinit var binding: UserRowLayout

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val ly = LayoutInflater.from(parent.context)
        binding = DataBindingUtil.inflate(ly, R.layout.user_row_layout, parent, false)
        return MyViewHolder(binding, context)

    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val user: AlbumsItem = userItem[position]
        Log.d("tagged", "Inside Adapter" + user)
        holder.userRow.user = user
        holder.userRow.executePendingBindings()
        holder.btnClick(holder.adapterPosition + 1)
    }

    override fun getItemCount(): Int {
        return userItem.size

    }

    inner class MyViewHolder(var userRow: UserRowLayout, var x: Context) :
        RecyclerView.ViewHolder(userRow.root) {
        fun btnClick(pos: Int) {
            userRow.imageView2.setOnClickListener {
                x.startActivity(Intent(x, DetailActivity::class.java))
val sharedPreferences= x.getSharedPreferences("id" ,  Context.MODE_PRIVATE)
                val editor = sharedPreferences.edit()
                editor.putInt("imageid",pos)
                editor.apply()
            }
        }
    }
}