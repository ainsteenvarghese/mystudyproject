package com.example.dummyapp

import android.view.View
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import coil.load

@BindingAdapter("loadApiImage")
fun ImageView.loadApiImage(imageUrl: String){
    this.load(imageUrl)

}



