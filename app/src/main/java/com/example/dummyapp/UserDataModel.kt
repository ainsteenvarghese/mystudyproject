package com.example.dummyapp

data class UserDataModel(
    var name: String? = null ,
    var address: String? = null
) {
}